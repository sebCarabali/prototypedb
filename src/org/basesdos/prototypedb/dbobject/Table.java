package org.basesdos.prototypedb.dbobject;

import java.util.List;

public class Table extends DbObject{

  public static final int TAM_NOMBRE = 15;
  
  private List<Column> listaColumnas;

  public Table() {}

  public Table(List<Column> listaColumnas) {
    this.listaColumnas = listaColumnas;
  }

  public List<Column> getListaColumnas() {
    return listaColumnas;
  }

  public void setListaColumnas(List<Column> listaColumnas) {
    this.listaColumnas = listaColumnas;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Table [listaColumnas=");
    builder.append(listaColumnas);
    builder.append(", getName()=");
    builder.append(getName());
    builder.append("]");
    return builder.toString();
  }
}
