/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.basesdos.prototypedb.storage.impl;

import java.io.IOException;
import java.io.RandomAccessFile;
import org.basesdos.prototypedb.dbobject.Column;
import org.basesdos.prototypedb.dbobject.DbObject;
import org.basesdos.prototypedb.dbobject.TipoDato;
import org.basesdos.prototypedb.exceptions.PrototypeDBException;
import org.basesdos.prototypedb.storage.IoUtils;
import org.basesdos.prototypedb.storage.Reader;

/**
 * Lee la definición de una columna desde el archivo de metadados de la tabla a
 * la que pertenece.
 *
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class ColumnReader implements Reader {

    private final RandomAccessFile raf;

    public ColumnReader(RandomAccessFile raf) {
        this.raf = raf;
    }

    @Override
    public DbObject read() throws IOException , PrototypeDBException{
        final Column dbObject = new Column();
        dbObject.setName(IoUtils.readString(raf, Column.TAM_NOMBRE));
        final String tipoDato = IoUtils.readString(raf, Column.TAM_TIPO_DATO);
        dbObject.setTipoDato(TipoDato.valueOf(tipoDato));
        dbObject.setNullable(raf.readBoolean());
        dbObject.setPrimaryKey(raf.readBoolean());
        return dbObject;
    }

}
