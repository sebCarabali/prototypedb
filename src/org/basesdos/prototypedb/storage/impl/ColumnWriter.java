/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.basesdos.prototypedb.storage.impl;

import java.io.IOException;
import java.io.RandomAccessFile;
import org.basesdos.prototypedb.dbobject.Column;
import org.basesdos.prototypedb.exceptions.PrototypeDBException;
import org.basesdos.prototypedb.storage.IoUtils;
import org.basesdos.prototypedb.storage.Writer;

/**
 * Escribe los datos de la definición de una columna en un archivo.
 * 
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class ColumnWriter implements Writer{

    
    /**
     * Columna que se escribirá.
     */
    private final Column columna;
    
    /**
     * Archivo donde va a escribitr.
     */
    private final RandomAccessFile raf;

    public ColumnWriter(Column columna, RandomAccessFile raf) {
        this.columna = columna;
        this.raf = raf;
    }
    
    @Override
    public void write() throws IOException , PrototypeDBException{
        final String tipoDato = String.valueOf(columna.getTipoDato());
        IoUtils.writeString(raf, columna.getName(), Column.TAM_NOMBRE);
        IoUtils.writeString(raf, tipoDato, Column.TAM_TIPO_DATO);
        raf.writeBoolean(columna.isNullable());
        raf.writeBoolean(columna.isPrimaryKey());
    }

}
