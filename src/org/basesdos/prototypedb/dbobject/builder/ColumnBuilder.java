package org.basesdos.prototypedb.dbobject.builder;

import org.basesdos.prototypedb.dbobject.Column;
import org.basesdos.prototypedb.dbobject.TipoDato;

public class ColumnBuilder implements IBuilder {

  private Column columnToBuild;
  
  public ColumnBuilder() {
    columnToBuild = new Column();
  }

  public void setName(String name) {
    columnToBuild.setName(name);
  }

  public void setTipoDato(TipoDato tipoDato) {
    columnToBuild.setTipoDato(tipoDato);
  }

  public void setNullable(boolean isNullable) {
    columnToBuild.setNullable(isNullable);
  }

  public void setPrimaryKey(boolean isPrimaryKey) {
    columnToBuild.setPrimaryKey(isPrimaryKey);
  }

  @Override
  public Column build() {
    return columnToBuild;
  }

}
