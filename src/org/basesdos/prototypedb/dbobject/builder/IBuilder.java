package org.basesdos.prototypedb.dbobject.builder;

import org.basesdos.prototypedb.dbobject.DbObject;

public interface IBuilder {
  public DbObject build();
}
