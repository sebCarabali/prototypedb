// Generated from SSql.g4 by ANTLR 4.5.3
package org.basesdos.prototypedb.sql.grammar;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SSqlParser}.
 */
public interface SSqlListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SSqlParser#sentencia}.
	 * @param ctx the parse tree
	 */
	void enterSentencia(SSqlParser.SentenciaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SSqlParser#sentencia}.
	 * @param ctx the parse tree
	 */
	void exitSentencia(SSqlParser.SentenciaContext ctx);
	/**
	 * Enter a parse tree produced by {@link SSqlParser#seleccionar}.
	 * @param ctx the parse tree
	 */
	void enterSeleccionar(SSqlParser.SeleccionarContext ctx);
	/**
	 * Exit a parse tree produced by {@link SSqlParser#seleccionar}.
	 * @param ctx the parse tree
	 */
	void exitSeleccionar(SSqlParser.SeleccionarContext ctx);
	/**
	 * Enter a parse tree produced by {@link SSqlParser#tipo_seleccion}.
	 * @param ctx the parse tree
	 */
	void enterTipo_seleccion(SSqlParser.Tipo_seleccionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SSqlParser#tipo_seleccion}.
	 * @param ctx the parse tree
	 */
	void exitTipo_seleccion(SSqlParser.Tipo_seleccionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SSqlParser#actualizar}.
	 * @param ctx the parse tree
	 */
	void enterActualizar(SSqlParser.ActualizarContext ctx);
	/**
	 * Exit a parse tree produced by {@link SSqlParser#actualizar}.
	 * @param ctx the parse tree
	 */
	void exitActualizar(SSqlParser.ActualizarContext ctx);
	/**
	 * Enter a parse tree produced by {@link SSqlParser#crear_tabla}.
	 * @param ctx the parse tree
	 */
	void enterCrear_tabla(SSqlParser.Crear_tablaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SSqlParser#crear_tabla}.
	 * @param ctx the parse tree
	 */
	void exitCrear_tabla(SSqlParser.Crear_tablaContext ctx);
	/**
	 * Enter a parse tree produced by {@link SSqlParser#insertar}.
	 * @param ctx the parse tree
	 */
	void enterInsertar(SSqlParser.InsertarContext ctx);
	/**
	 * Exit a parse tree produced by {@link SSqlParser#insertar}.
	 * @param ctx the parse tree
	 */
	void exitInsertar(SSqlParser.InsertarContext ctx);
	/**
	 * Enter a parse tree produced by {@link SSqlParser#creartabla}.
	 * @param ctx the parse tree
	 */
	void enterCreartabla(SSqlParser.CreartablaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SSqlParser#creartabla}.
	 * @param ctx the parse tree
	 */
	void exitCreartabla(SSqlParser.CreartablaContext ctx);
	/**
	 * Enter a parse tree produced by {@link SSqlParser#eliminar_tabla}.
	 * @param ctx the parse tree
	 */
	void enterEliminar_tabla(SSqlParser.Eliminar_tablaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SSqlParser#eliminar_tabla}.
	 * @param ctx the parse tree
	 */
	void exitEliminar_tabla(SSqlParser.Eliminar_tablaContext ctx);
	/**
	 * Enter a parse tree produced by {@link SSqlParser#lista_columnas}.
	 * @param ctx the parse tree
	 */
	void enterLista_columnas(SSqlParser.Lista_columnasContext ctx);
	/**
	 * Exit a parse tree produced by {@link SSqlParser#lista_columnas}.
	 * @param ctx the parse tree
	 */
	void exitLista_columnas(SSqlParser.Lista_columnasContext ctx);
	/**
	 * Enter a parse tree produced by {@link SSqlParser#definicion_de_columna}.
	 * @param ctx the parse tree
	 */
	void enterDefinicion_de_columna(SSqlParser.Definicion_de_columnaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SSqlParser#definicion_de_columna}.
	 * @param ctx the parse tree
	 */
	void exitDefinicion_de_columna(SSqlParser.Definicion_de_columnaContext ctx);
	/**
	 * Enter a parse tree produced by {@link SSqlParser#condicion}.
	 * @param ctx the parse tree
	 */
	void enterCondicion(SSqlParser.CondicionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SSqlParser#condicion}.
	 * @param ctx the parse tree
	 */
	void exitCondicion(SSqlParser.CondicionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SSqlParser#tipo_dato}.
	 * @param ctx the parse tree
	 */
	void enterTipo_dato(SSqlParser.Tipo_datoContext ctx);
	/**
	 * Exit a parse tree produced by {@link SSqlParser#tipo_dato}.
	 * @param ctx the parse tree
	 */
	void exitTipo_dato(SSqlParser.Tipo_datoContext ctx);
}