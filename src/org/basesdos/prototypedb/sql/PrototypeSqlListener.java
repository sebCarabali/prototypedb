/**
 *
 */
package org.basesdos.prototypedb.sql;

import org.basesdos.prototypedb.dbobject.ResultSet;
import org.basesdos.prototypedb.dbobject.TipoDato;
import org.basesdos.prototypedb.dbobject.builder.ColumnBuilder;
import org.basesdos.prototypedb.dbobject.builder.TableBuilder;
import org.basesdos.prototypedb.exceptions.PrototypeDBException;
import org.basesdos.prototypedb.sql.grammar.SSqlBaseListener;
import org.basesdos.prototypedb.sql.grammar.SSqlParser;
import org.basesdos.prototypedb.sql.grammar.SSqlParser.CreartablaContext;
import org.basesdos.prototypedb.sql.grammar.SSqlParser.Definicion_de_columnaContext;
import org.basesdos.prototypedb.sql.grammar.SSqlParser.Eliminar_tablaContext;
import org.basesdos.prototypedb.sql.grammar.SSqlParser.InsertarContext;

/**
 * @author Sebastián Carabali (sebastiancc@unicauca.edu.co)
 *
 */
public class PrototypeSqlListener extends SSqlBaseListener {

    private String mensaje;

    private ResultSet rs;

    @Override
    public void enterCreartabla(CreartablaContext ctx) {
        TableBuilder tblBuilder = new TableBuilder();
        tblBuilder.setName(ctx.ID().getText());
        for (Definicion_de_columnaContext deffColCtx : ctx.definicion_de_columna()) {
            ColumnBuilder colBuilder = new ColumnBuilder();
            colBuilder.setName(deffColCtx.ID().getText());
            System.out.println("TIPO DE DATO: " + TipoDato.valueOf(deffColCtx.DEF_TIPO_DATO().getText()));
            colBuilder.setTipoDato(TipoDato.valueOf(deffColCtx.DEF_TIPO_DATO().getText()));
            colBuilder.setNullable((deffColCtx.NO_NULO() == null));
            colBuilder.setPrimaryKey((deffColCtx.LLAVE_PRIMARIA() != null));
            tblBuilder.addColumn(colBuilder.build());
        }
        CreateTableStatement createStmt = new CreateTableStatement(tblBuilder.build());
        mensaje = createStmt.execute();
    }

    @Override
    public void enterInsertar(InsertarContext ctx) {
        InsertStatement insertStatement = new InsertStatement();
        final String tableName = ctx.ID().getText();
        ctx.tipo_dato().stream().forEach((tipoCtx)
                -> {
            insertStatement.addValor(tipoCtx.getText());
        });
        insertStatement.setNameTable(tableName);
        insertStatement.execute();
    }

    @Override
    public void enterEliminar_tabla(Eliminar_tablaContext ctx) {
        final String tableName = ctx.ID().getText();
        final DropTableStatement stmt = new DropTableStatement(tableName);
        mensaje = stmt.execute();
    }

    @Override
    public void enterSeleccionar(SSqlParser.SeleccionarContext ctx) {
        try {
            final boolean todo = ctx.tipo_seleccion().TODO() != null;
            final String from = ctx.ID().getText();
            final SelectStatement selectStatement;
            if(ctx.condicion() != null ) {
                final String operadorLogico = ctx.condicion().OPERADOR_LOGICO().getText();
                final String columna = ctx.condicion().ID().getText();
                final String valor = ctx.condicion().tipo_dato().getText();
                final Condicion condicion = new Condicion(operadorLogico, columna, valor);
                selectStatement = new SelectStatement(from, todo, condicion);
            } else {
                selectStatement = new SelectStatement(from, todo, null);
            }
            rs = selectStatement.execute();
            mensaje = "Consulta realizada exitosamente";
        } catch (PrototypeDBException ex) {
            mensaje = ex.getMessage();
        }
    }

    public String getMensaje() {
        return mensaje;
    }

    public ResultSet getRs() {
        return rs;
    }

}
