/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.basesdos.prototypedb.storage.impl;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import org.basesdos.prototypedb.dbobject.Column;
import org.basesdos.prototypedb.dbobject.Table;
import org.basesdos.prototypedb.exceptions.PrototypeDBException;
import org.basesdos.prototypedb.storage.IoUtils;
import org.basesdos.prototypedb.storage.Writer;

/**
 * Escribe la definición de una tabla y de cada una de sus columnas.
 * 
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class TableWriter implements Writer{

    private final Table tabla;

    public TableWriter(Table tabla) {
        this.tabla = tabla;
    }
    
    @Override
    public void write() throws IOException, PrototypeDBException {
        final String filePath = "data/tables";
        final File file = new File(filePath);
        if(!file.exists()) {
            file.mkdir();
        }
        final RandomAccessFile raf = new RandomAccessFile("data/tables/"+tabla.getName().toLowerCase()+".data", "rw");
        // Se imprime el nombre de la tabla.
        IoUtils.writeString(raf, tabla.getName(), Table.TAM_NOMBRE);
        // Se imprimen los datos de cada una de las columnas de la tabla.
        for(Column col : tabla.getListaColumnas())
        {
            ColumnWriter cw = new ColumnWriter(col, raf);
            cw.write();
        }
        raf.close();
    }

}
