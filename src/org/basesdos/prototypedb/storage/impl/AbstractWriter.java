/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.basesdos.prototypedb.storage.impl;

import org.basesdos.prototypedb.dbobject.DbObject;
import org.basesdos.prototypedb.storage.Writer;

/**
 *
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public abstract class AbstractWriter implements Writer
{
    private final DbObject objetoToWrite;

    public AbstractWriter(DbObject objetoToWrite) {
        this.objetoToWrite = objetoToWrite;
    }
}
