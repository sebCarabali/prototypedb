grammar SSql;

sentencia
	: seleccionar
	| actualizar
	| crear_tabla
	| creartabla
	| insertar
	| eliminar_tabla
	/*| eliminar*/
	;

seleccionar
	: SELECCIONAR tipo_seleccion DE ID (DONDE condicion ( (AND | OR) condicion)*)? (ORDENAR_POR ID (DESC | ASC))*	
	;

tipo_seleccion
	: TODO 
	| lista_columnas
	;
	
actualizar
	: ACTUALIZAR ID ESTABLECER condicion ( ',' condicion)* (DONDE condicion)?
	;
	
crear_tabla
	: CREAR TABLA ID '(' definicion_de_columna (',' definicion_de_columna)* ')'
	;

insertar
	: ('insertar' | 'INSERTAR') ('en' | 'EN') ID ('valores' | 'VALORES') '(' tipo_dato ( ',' tipo_dato)* ')' 
	;

creartabla
	: ('crear' | 'CREAR') ( 'tabla' | 'TABLA') ID '(' definicion_de_columna (',' definicion_de_columna)* ')'
	;

eliminar_tabla
	: ('eliminar' | 'ELIMINAR') ( 'tabla' | 'TABLA') ID
	;

lista_columnas
	: ID ( ',' ID)*
	;

definicion_de_columna
	: ID DEF_TIPO_DATO (NULO | NO_NULO)? (LLAVE_PRIMARIA)?
	;

condicion
    : ID
    | tipo_dato
    | condicion OPERADOR_LOGICO condicion
    | condicion AND condicion
    | condicion OR condicion
    | '(' condicion ')'
    ;

ID
	: [a-z]+
	;
	
SELECCIONAR
	: 'seleccionar'| 'SELECCIONAR'
	;

ACTUALIZAR
	: 'actualizar' | 'ACTUALIZAR'
	;

ESTABLECER
	: 'establecer' | 'ESTABLECER'
	;

CREAR
    : ('crear' | 'CREAR')
    ;

DONDE
    : 'donde' | 'DONDE'
    ;

TABLA
	: ('tabla' | 'TABLA')
	;    

LLAVE_PRIMARIA
	: 'llave_primaria' | 'LLAVE_PRIMARIA'
	;

NULO
	: 'nulo' | 'NULO'
	;

NO_NULO
	: 'no_nulo'
	| 'NO_NULO'
	;

TODO 
	: '*'
	;


DE
	: 'de' | 'DE'
	;
	
AND
    : 'y' | 'Y'
    | '&&'
    ;

OR
    : 'o' | 'O'
    | '||'
    ;

ORDENAR_POR
	: 'ordenar_por' | 'ORDENAR_POR'
	;
	
DESC
	: 'desc' | 'DESC'
	;
	
ASC
	:'asc' | 'ASC'
	;

tipo_dato
    :ENTERO
    |CADENA
    ;

DEF_TIPO_DATO
    :'entero'  | 'ENTERO'
    |'cadena' | 'CADENA'
    |'real' | 'REAL'
    ;

ENTERO
    :[0-9]+
    ;

fragment 
ESCAPED_QUOTE : '\\"';
CADENA :   '"' ( ESCAPED_QUOTE | ~('\n'|'\r') )*? '"';

OPERADOR_LOGICO
    : '='
    | '>'
    | '<'
    | '>='
    | '<='
    | '<>'
    ;


WS : [ \t\r\n]+ -> skip;
