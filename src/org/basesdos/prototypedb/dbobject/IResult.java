/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.basesdos.prototypedb.dbobject;

/**
 * Respresenta los resultados.
 *
 * @author Personal
 */
public interface IResult {

    /**
     * Obtiene los nombres de las columnas retornadas por la consulta.
     *
     * @param column
     * @return Nombre de la columna
     * @throws org.basesdos.prototypedb.dbobject.QueryException
     */
    public String getColumnName(int column) throws QueryException;

    /**
     * Obtiene la cantidad de filas alteradas por la consulta.
     *
     * @return Cantidad de filas en el resultado.
     */
    public int getRowCount();

    /**
     * Obtiene la cantidad de columnas obtenidas en el resultado.
     *
     * @return Cantidad de columnas.
     * @throws org.basesdos.prototypedb.dbobject.QueryException
     */
    public int getColumnCount() throws QueryException;

    /**
     * Obtiene el resultado de la consulta.
     *
     * @param row Fila
     * @param column Columna
     * @return
     * @throws org.basesdos.prototypedb.dbobject.QueryException
     */
    public Object getValueAt(int row, int column) throws QueryException;
}
