package org.basesdos.prototypedb.exceptions;

public class TableNotExistExeption extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -49845652163084806L;

	public TableNotExistExeption(String tableName) {
		super(String.format("La tabla %s no existe.", tableName));
	}
}
