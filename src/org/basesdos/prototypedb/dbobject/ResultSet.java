/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.basesdos.prototypedb.dbobject;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class ResultSet extends DbObject implements IResult{

    private List<RowSet> filas;
    
    private int numColumnas;

    public ResultSet() {
        numColumnas = 0;
    }

    
    public List<RowSet> getListaDatos() {
        return filas;
    }
    
    public void addResult(RowSet result)
    {
        if(filas == null)
            filas = new ArrayList<>();
        numColumnas = result.getNumColumna();
        filas.add(result);
    }

    @Override
    public String getColumnName(int column) throws QueryException {
        if(filas != null && !filas.isEmpty()) {
            return filas.get(0).getCols().get(0).getColumnName();
        }
        return null;
    }

    @Override
    public int getRowCount() {
        return filas.size();
    }

    @Override
    public int getColumnCount() throws QueryException {
        return numColumnas;
    }

    @Override
    public Object getValueAt(int row, int column) throws QueryException {
        if(filas != null && !filas.isEmpty()) {
            return filas.get(row).getCols().get(column).getData();
        }
        return null;
    }
}
