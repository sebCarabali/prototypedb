/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.basesdos.prototypedb.storage;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Clase de utilidad para escribir y leer cadena en un archivo de acceso 
 * aleatorio. 
 * 
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class IoUtils 
{
    /**
     * Escribe una cadena en el archivo.
     * 
     * @param out Archivo donde se escribirá la cadena.
     * @param str Cadena a escribir
     * @param length Tamaño en bytes de la cadena.
     * @throws IOException 
     */
    public static void writeString(RandomAccessFile out, String str, int length) throws IOException
    {
        final StringBuilder buffer;
        buffer = new StringBuilder(str);
        buffer.setLength(length);
        out.writeChars(buffer.toString());
    }

    /**
     * Lee una cadena desde un archivo.
     * 
     * @param in Archivo desde donde se leerá la cadena.
     * @param length Tamaño en bytes de la cadena.
     * @return Cadena leida desde el archivo.
     * @throws IOException 
     */
    public static String readString(RandomAccessFile in, int length) throws IOException
    {
        char campo[] = new char[length];
        for (int i = 0; i < length; i++)
        {
            campo[i] = in.readChar();
        }
        String cadena = new String(campo).replace('\0', ';');
        int index = cadena.indexOf(';');
        if (index != -1)
        {
            return cadena.substring(0, index);
        }
        return cadena;
    }
}
