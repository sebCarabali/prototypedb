package org.basesdos.prototypedb.sql;

import java.io.IOException;

//import org.basesdos.prototypedb.filemanagement.DbFileUtil;
import org.basesdos.prototypedb.exceptions.TableNotExistExeption;

public class DropTableStatement implements ISSqlStatement {

	private final String tableName;
	
	public DropTableStatement(String tableName) {
		this.tableName = tableName;
	}
	
	@Override
	public String execute() {
//		try {
//			DbFileUtil.dropTableFile(tableName);
//		} catch (IOException e) {
//			return "Error al abrir el archivo.";
//		} catch (TableNotExistExeption e) {
//			return e.getMessage();
//		}
		return "Tabla eliminada.";
	}

}
