/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.basesdos.prototypedb.sql;

/**
 *
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class Condicion {

    private final boolean isUnary;
    private final String comparator;
    private final String op1;
    private final String op2;

    public Condicion(boolean isUnary, String comparator, String op1, String op2) {
        this.isUnary = isUnary;
        this.comparator = comparator;
        this.op1 = op1;
        this.op2 = op2;
    }

    public boolean isIsUnary() {
        return isUnary;
    }

    public String getComparator() {
        return comparator;
    }

    public String getOp1() {
        return op1;
    }

    public String getOp2() {
        return op2;
    }

}
