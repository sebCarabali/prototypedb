/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.basesdos.prototypedb.dbobject;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class RowSet extends DbObject {

    private List<ResultValue> cols;

    private int numColumna;

    public RowSet() {
        this.numColumna = 0;
    }

    public void addColumna(ResultValue rv) {
        if(cols == null)
            cols = new ArrayList<>();
        cols.add(rv);
        numColumna++;
    }

    public List<ResultValue> getCols() {
        return cols;
    }

    public int getNumColumna() {
        return numColumna;
    }

}
