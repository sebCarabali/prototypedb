package org.basesdos.prototypedb.dbobject.builder;

import java.util.ArrayList;

import org.basesdos.prototypedb.dbobject.Column;
import org.basesdos.prototypedb.dbobject.Table;

public class TableBuilder implements IBuilder {

  private Table tableToBuild;
  
  public TableBuilder() {
    tableToBuild = new Table();
  }

  public void setName(String name) {
    tableToBuild.setName(name);
  }

  public void addColumn(Column newColumn) {
    if(tableToBuild.getListaColumnas() == null) {
      tableToBuild.setListaColumnas(new ArrayList<>());
    }
    tableToBuild.getListaColumnas().add(newColumn);
  }

  @Override
  public Table build() {
    return tableToBuild;
  }

}
