/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.basesdos.prototypedb.interfaceDBMS;

import java.io.IOException;
import org.basesdos.prototypedb.dbobject.ResultSet;
import org.basesdos.prototypedb.exceptions.PrototypeDBException;
import org.basesdos.prototypedb.storage.Reader;
import org.basesdos.prototypedb.storage.impl.DataReader;

/**
 *
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class Test {

    public static void main(String[] args) throws IOException, PrototypeDBException {
//        TableReader tr = new TableReader("prueba");
//        Table t = (Table) tr.read();
//        System.out.println("Tabla: " + t);

//        List<String> valores = new ArrayList<>();
//        valores.add("1");
//        valores.add("\"Esto es otra cosa bitch\"");
//        valores.add("657.3");
//        Writer dw = new DataWriter("prueba", valores);
//        dw.write();

        final Reader dr = new DataReader("prueba");
        final ResultSet rs = (ResultSet) dr.read();
        rs.getListaDatos().forEach((fila) -> {
            fila.getCols().forEach((col) -> {
                System.out.println("ColName: " + col.getColumnName());
                System.out.println("Data: " + col.getData());
            });
        });
    }

}
