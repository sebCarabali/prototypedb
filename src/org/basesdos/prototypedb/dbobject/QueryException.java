/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.basesdos.prototypedb.dbobject;

import org.basesdos.prototypedb.exceptions.PrototypeDBException;

/**
 *
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class QueryException extends PrototypeDBException {

    public QueryException(String mensaje) {
        super(mensaje);
    }

}
