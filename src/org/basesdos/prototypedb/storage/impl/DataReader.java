/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.basesdos.prototypedb.storage.impl;

import java.io.IOException;
import java.io.RandomAccessFile;
import org.basesdos.prototypedb.dbobject.Column;
import org.basesdos.prototypedb.dbobject.DbObject;
import org.basesdos.prototypedb.dbobject.ResultSet;
import org.basesdos.prototypedb.dbobject.ResultValue;
import org.basesdos.prototypedb.dbobject.RowSet;
import org.basesdos.prototypedb.dbobject.Table;
import org.basesdos.prototypedb.dbobject.TipoDato;
import org.basesdos.prototypedb.dbobject.TipoDatoLength;
import org.basesdos.prototypedb.exceptions.PrototypeDBException;
import org.basesdos.prototypedb.sql.Condicion;
import org.basesdos.prototypedb.storage.IoUtils;
import org.basesdos.prototypedb.storage.Reader;

/**
 *
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class DataReader implements Reader {

    private final String tableName;

    private Condicion condicion;

    public DataReader(String tableName) {
        this.tableName = tableName;
    }

    public DataReader(String tableName, Condicion condicion) {
        this.tableName = tableName;
        this.condicion = condicion;
    }

    @Override
    public DbObject read() throws IOException, PrototypeDBException {
        final ResultSet res = new ResultSet();
        final TableReader tr = new TableReader(tableName);
        final Table tabla = (Table) tr.read();
        final RandomAccessFile raf = new RandomAccessFile("data/tables/" + tabla.getName() + "datos.data", "rw");
        boolean colExist = true;
        raf.seek(0);
        while (raf.getFilePointer() < raf.length()) {
            final RowSet rs = new RowSet();
            boolean insertar = false;
            for (Column col : tabla.getListaColumnas()) {

                ResultValue rv = new ResultValue();
                rv.setColumnName(col.getName());
                switch (col.getTipoDato()) {
                    case ENTERO:
                        rv.setData(raf.readInt());
                        break;
                    case REAL:
                        rv.setData(raf.readInt());
                        break;
                    case CADENA:
                        rv.setData(IoUtils.readString(raf, TipoDatoLength.TAM_CADENA));
                        break;
                }
                if (condicion != null) {
                    if (col.getName().equalsIgnoreCase(condicion.getColumna())) {
                        colExist = true;
                        if (ejecutarCondicion(rv.getData(), condicion.getComparator(), condicion.getValor(), col.getTipoDato())) {
                            insertar = true;
                        }
                    } else {
                        colExist = false;
                    }
                } else {

                }
                rs.addColumna(rv);
            }
            
            if(!colExist)
                throw new PrototypeDBException("La columna " + condicion.getColumna() + " No existe");
            
            if (insertar) {
                res.addResult(rs);
            }
        }
        return res;
    }

    private boolean ejecutarCondicion(Object valor1, String op, String valor2, TipoDato tipoDato) {

        if (tipoDato == TipoDato.ENTERO) {

            switch (op) {
                case "=":
                    return Integer.valueOf(valor2).compareTo((int) valor1) == 0;
                case "<":
                    return Integer.valueOf(valor2).compareTo((int) valor1) > 0;
                case ">":
                    return Integer.valueOf(valor2).compareTo((int) valor1) < 0;
                default:
                    return false;
            }
        } else {
            switch (op) {
                case "=":
                    return valor2.compareTo((String) valor1) == 0;
                case "<":
                    return valor2.compareTo((String) valor1) > 0;
                case ">":
                    return valor2.compareTo((String) valor1) < 0;
                default:
                    return false;
            }
        }
    }

}
