/**
 *
 */
package org.basesdos.prototypedb.dbobject;

/**
 * @author Sebastián Carabali (sebastiancc@unicauca.edu.co)
 *
 */
public class Column extends DbObject {

    /**
     * Tamaño del nombre.
     */
    public static final int TAM_NOMBRE = 30;

    /**
     * Tamaño del tipo de dato
     */
    public static final int TAM_TIPO_DATO = 6;

    /**
     * Tamaño del is nullable.
     */
    public static final int TAM_IS_NULLABLE = 1;

    /**
     * Tamaño del is primary key.
     */
    public static final int TAM_IS_PRIMARY_KEY = 1;
    
    public static final long SIZE = TAM_NOMBRE * 2 + TAM_TIPO_DATO * 2 + TAM_IS_NULLABLE + TAM_IS_PRIMARY_KEY;

    private TipoDato tipoDato;
    private boolean isNullable;
    private boolean isPrimaryKey;
    private int size;

    public Column() {
    }

    public Column(TipoDato tipoDato, boolean isNullable, boolean isPrimaryKey, int size) {
        this.tipoDato = tipoDato;
        this.isNullable = isNullable;
        this.isPrimaryKey = isPrimaryKey;
        this.size = size;
    }

    public TipoDato getTipoDato() {
        return tipoDato;
    }

    public boolean isNullable() {
        return isNullable;
    }

    public boolean isPrimaryKey() {
        return isPrimaryKey;
    }

    public void setTipoDato(TipoDato tipoDato) {
        this.tipoDato = tipoDato;
    }

    public void setNullable(boolean isNullable) {
        this.isNullable = isNullable;
    }

    public void setPrimaryKey(boolean isPrimaryKey) {
        this.isPrimaryKey = isPrimaryKey;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Column [tipoDato=");
        builder.append(tipoDato);
        builder.append(", isNullable=");
        builder.append(isNullable);
        builder.append(", isPrimaryKey=");
        builder.append(isPrimaryKey);
        builder.append(", getName()=");
        builder.append(getName());
        builder.append("]");
        return builder.toString();
    }

}
