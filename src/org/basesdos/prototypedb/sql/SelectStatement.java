/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.basesdos.prototypedb.sql;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.basesdos.prototypedb.dbobject.ResultSet;
import org.basesdos.prototypedb.exceptions.PrototypeDBException;
import org.basesdos.prototypedb.storage.impl.DataReader;

/**
 *
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class SelectStatement implements ISSqlStatement
{
    
    private final String from;
    private final boolean todo;
    private final List<String> codiciones;

    public SelectStatement(String from, boolean todo, List<String> codiciones) {
        this.from = from;
        this.todo = todo;
        this.codiciones = codiciones;
    }
    
    @Override
    public String execute() {
        try {
            final DataReader dr = new DataReader(from);
            final ResultSet rs = (ResultSet) dr.read();
            
            return null;
        } catch (IOException ex) {
            Logger.getLogger(SelectStatement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PrototypeDBException ex) {
            Logger.getLogger(SelectStatement.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
