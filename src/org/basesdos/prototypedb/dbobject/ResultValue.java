/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.basesdos.prototypedb.dbobject;

/**
 *
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class ResultValue {

    private String columnName;

    private Object data;

    public ResultValue() {
    }

    public ResultValue(String columnName, Object data) {
        this.columnName = columnName;
        this.data = data;
    }

    public String getColumnName() {
        return columnName;
    }

    public Object getData() {
        return data;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
