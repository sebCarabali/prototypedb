package org.basesdos.prototypedb.sql;

public interface ISSqlStatement 
{
	/**
	 * Ejecuta la sentincia para interactuar con el motor de bases de datos
	 * @return Mensaje de resultado
	 */
  public String execute();
}
