// Generated from SSql.g4 by ANTLR 4.5.3
package org.basesdos.prototypedb.sql.grammar;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SSqlParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, ID=16, SELECCIONAR=17, 
		ACTUALIZAR=18, ESTABLECER=19, CREAR=20, DONDE=21, TABLA=22, LLAVE_PRIMARIA=23, 
		NULO=24, NO_NULO=25, TODO=26, DE=27, AND=28, OR=29, ORDENAR_POR=30, DESC=31, 
		ASC=32, DEF_TIPO_DATO=33, ENTERO=34, CADENA=35, OPERADOR_LOGICO=36, WS=37;
	public static final int
		RULE_sentencia = 0, RULE_seleccionar = 1, RULE_tipo_seleccion = 2, RULE_actualizar = 3, 
		RULE_crear_tabla = 4, RULE_insertar = 5, RULE_creartabla = 6, RULE_eliminar_tabla = 7, 
		RULE_lista_columnas = 8, RULE_definicion_de_columna = 9, RULE_condicion = 10, 
		RULE_tipo_dato = 11;
	public static final String[] ruleNames = {
		"sentencia", "seleccionar", "tipo_seleccion", "actualizar", "crear_tabla", 
		"insertar", "creartabla", "eliminar_tabla", "lista_columnas", "definicion_de_columna", 
		"condicion", "tipo_dato"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "','", "'('", "')'", "'insertar'", "'INSERTAR'", "'en'", "'EN'", 
		"'valores'", "'VALORES'", "'crear'", "'CREAR'", "'tabla'", "'TABLA'", 
		"'eliminar'", "'ELIMINAR'", null, null, null, null, null, null, null, 
		null, null, null, "'*'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, "ID", "SELECCIONAR", "ACTUALIZAR", "ESTABLECER", 
		"CREAR", "DONDE", "TABLA", "LLAVE_PRIMARIA", "NULO", "NO_NULO", "TODO", 
		"DE", "AND", "OR", "ORDENAR_POR", "DESC", "ASC", "DEF_TIPO_DATO", "ENTERO", 
		"CADENA", "OPERADOR_LOGICO", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SSql.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SSqlParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class SentenciaContext extends ParserRuleContext {
		public SeleccionarContext seleccionar() {
			return getRuleContext(SeleccionarContext.class,0);
		}
		public ActualizarContext actualizar() {
			return getRuleContext(ActualizarContext.class,0);
		}
		public Crear_tablaContext crear_tabla() {
			return getRuleContext(Crear_tablaContext.class,0);
		}
		public CreartablaContext creartabla() {
			return getRuleContext(CreartablaContext.class,0);
		}
		public InsertarContext insertar() {
			return getRuleContext(InsertarContext.class,0);
		}
		public Eliminar_tablaContext eliminar_tabla() {
			return getRuleContext(Eliminar_tablaContext.class,0);
		}
		public SentenciaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentencia; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).enterSentencia(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).exitSentencia(this);
		}
	}

	public final SentenciaContext sentencia() throws RecognitionException {
		SentenciaContext _localctx = new SentenciaContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_sentencia);
		try {
			setState(30);
			switch (_input.LA(1)) {
			case SELECCIONAR:
				enterOuterAlt(_localctx, 1);
				{
				setState(24);
				seleccionar();
				}
				break;
			case ACTUALIZAR:
				enterOuterAlt(_localctx, 2);
				{
				setState(25);
				actualizar();
				}
				break;
			case CREAR:
				enterOuterAlt(_localctx, 3);
				{
				setState(26);
				crear_tabla();
				}
				break;
			case T__9:
			case T__10:
				enterOuterAlt(_localctx, 4);
				{
				setState(27);
				creartabla();
				}
				break;
			case T__3:
			case T__4:
				enterOuterAlt(_localctx, 5);
				{
				setState(28);
				insertar();
				}
				break;
			case T__13:
			case T__14:
				enterOuterAlt(_localctx, 6);
				{
				setState(29);
				eliminar_tabla();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SeleccionarContext extends ParserRuleContext {
		public TerminalNode SELECCIONAR() { return getToken(SSqlParser.SELECCIONAR, 0); }
		public Tipo_seleccionContext tipo_seleccion() {
			return getRuleContext(Tipo_seleccionContext.class,0);
		}
		public TerminalNode DE() { return getToken(SSqlParser.DE, 0); }
		public List<TerminalNode> ID() { return getTokens(SSqlParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(SSqlParser.ID, i);
		}
		public TerminalNode DONDE() { return getToken(SSqlParser.DONDE, 0); }
		public List<CondicionContext> condicion() {
			return getRuleContexts(CondicionContext.class);
		}
		public CondicionContext condicion(int i) {
			return getRuleContext(CondicionContext.class,i);
		}
		public List<TerminalNode> ORDENAR_POR() { return getTokens(SSqlParser.ORDENAR_POR); }
		public TerminalNode ORDENAR_POR(int i) {
			return getToken(SSqlParser.ORDENAR_POR, i);
		}
		public List<TerminalNode> DESC() { return getTokens(SSqlParser.DESC); }
		public TerminalNode DESC(int i) {
			return getToken(SSqlParser.DESC, i);
		}
		public List<TerminalNode> ASC() { return getTokens(SSqlParser.ASC); }
		public TerminalNode ASC(int i) {
			return getToken(SSqlParser.ASC, i);
		}
		public List<TerminalNode> AND() { return getTokens(SSqlParser.AND); }
		public TerminalNode AND(int i) {
			return getToken(SSqlParser.AND, i);
		}
		public List<TerminalNode> OR() { return getTokens(SSqlParser.OR); }
		public TerminalNode OR(int i) {
			return getToken(SSqlParser.OR, i);
		}
		public SeleccionarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_seleccionar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).enterSeleccionar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).exitSeleccionar(this);
		}
	}

	public final SeleccionarContext seleccionar() throws RecognitionException {
		SeleccionarContext _localctx = new SeleccionarContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_seleccionar);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(32);
			match(SELECCIONAR);
			setState(33);
			tipo_seleccion();
			setState(34);
			match(DE);
			setState(35);
			match(ID);
			setState(45);
			_la = _input.LA(1);
			if (_la==DONDE) {
				{
				setState(36);
				match(DONDE);
				setState(37);
				condicion(0);
				setState(42);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==AND || _la==OR) {
					{
					{
					setState(38);
					_la = _input.LA(1);
					if ( !(_la==AND || _la==OR) ) {
					_errHandler.recoverInline(this);
					} else {
						consume();
					}
					setState(39);
					condicion(0);
					}
					}
					setState(44);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(52);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ORDENAR_POR) {
				{
				{
				setState(47);
				match(ORDENAR_POR);
				setState(48);
				match(ID);
				setState(49);
				_la = _input.LA(1);
				if ( !(_la==DESC || _la==ASC) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				}
				}
				setState(54);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Tipo_seleccionContext extends ParserRuleContext {
		public TerminalNode TODO() { return getToken(SSqlParser.TODO, 0); }
		public Lista_columnasContext lista_columnas() {
			return getRuleContext(Lista_columnasContext.class,0);
		}
		public Tipo_seleccionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tipo_seleccion; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).enterTipo_seleccion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).exitTipo_seleccion(this);
		}
	}

	public final Tipo_seleccionContext tipo_seleccion() throws RecognitionException {
		Tipo_seleccionContext _localctx = new Tipo_seleccionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_tipo_seleccion);
		try {
			setState(57);
			switch (_input.LA(1)) {
			case TODO:
				enterOuterAlt(_localctx, 1);
				{
				setState(55);
				match(TODO);
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(56);
				lista_columnas();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ActualizarContext extends ParserRuleContext {
		public TerminalNode ACTUALIZAR() { return getToken(SSqlParser.ACTUALIZAR, 0); }
		public TerminalNode ID() { return getToken(SSqlParser.ID, 0); }
		public TerminalNode ESTABLECER() { return getToken(SSqlParser.ESTABLECER, 0); }
		public List<CondicionContext> condicion() {
			return getRuleContexts(CondicionContext.class);
		}
		public CondicionContext condicion(int i) {
			return getRuleContext(CondicionContext.class,i);
		}
		public TerminalNode DONDE() { return getToken(SSqlParser.DONDE, 0); }
		public ActualizarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_actualizar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).enterActualizar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).exitActualizar(this);
		}
	}

	public final ActualizarContext actualizar() throws RecognitionException {
		ActualizarContext _localctx = new ActualizarContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_actualizar);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(59);
			match(ACTUALIZAR);
			setState(60);
			match(ID);
			setState(61);
			match(ESTABLECER);
			setState(62);
			condicion(0);
			setState(67);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(63);
				match(T__0);
				setState(64);
				condicion(0);
				}
				}
				setState(69);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(72);
			_la = _input.LA(1);
			if (_la==DONDE) {
				{
				setState(70);
				match(DONDE);
				setState(71);
				condicion(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Crear_tablaContext extends ParserRuleContext {
		public TerminalNode CREAR() { return getToken(SSqlParser.CREAR, 0); }
		public TerminalNode TABLA() { return getToken(SSqlParser.TABLA, 0); }
		public TerminalNode ID() { return getToken(SSqlParser.ID, 0); }
		public List<Definicion_de_columnaContext> definicion_de_columna() {
			return getRuleContexts(Definicion_de_columnaContext.class);
		}
		public Definicion_de_columnaContext definicion_de_columna(int i) {
			return getRuleContext(Definicion_de_columnaContext.class,i);
		}
		public Crear_tablaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_crear_tabla; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).enterCrear_tabla(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).exitCrear_tabla(this);
		}
	}

	public final Crear_tablaContext crear_tabla() throws RecognitionException {
		Crear_tablaContext _localctx = new Crear_tablaContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_crear_tabla);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(74);
			match(CREAR);
			setState(75);
			match(TABLA);
			setState(76);
			match(ID);
			setState(77);
			match(T__1);
			setState(78);
			definicion_de_columna();
			setState(83);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(79);
				match(T__0);
				setState(80);
				definicion_de_columna();
				}
				}
				setState(85);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(86);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InsertarContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SSqlParser.ID, 0); }
		public List<Tipo_datoContext> tipo_dato() {
			return getRuleContexts(Tipo_datoContext.class);
		}
		public Tipo_datoContext tipo_dato(int i) {
			return getRuleContext(Tipo_datoContext.class,i);
		}
		public InsertarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insertar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).enterInsertar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).exitInsertar(this);
		}
	}

	public final InsertarContext insertar() throws RecognitionException {
		InsertarContext _localctx = new InsertarContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_insertar);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(88);
			_la = _input.LA(1);
			if ( !(_la==T__3 || _la==T__4) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(89);
			_la = _input.LA(1);
			if ( !(_la==T__5 || _la==T__6) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(90);
			match(ID);
			setState(91);
			_la = _input.LA(1);
			if ( !(_la==T__7 || _la==T__8) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(92);
			match(T__1);
			setState(93);
			tipo_dato();
			setState(98);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(94);
				match(T__0);
				setState(95);
				tipo_dato();
				}
				}
				setState(100);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(101);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CreartablaContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SSqlParser.ID, 0); }
		public List<Definicion_de_columnaContext> definicion_de_columna() {
			return getRuleContexts(Definicion_de_columnaContext.class);
		}
		public Definicion_de_columnaContext definicion_de_columna(int i) {
			return getRuleContext(Definicion_de_columnaContext.class,i);
		}
		public CreartablaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_creartabla; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).enterCreartabla(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).exitCreartabla(this);
		}
	}

	public final CreartablaContext creartabla() throws RecognitionException {
		CreartablaContext _localctx = new CreartablaContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_creartabla);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(103);
			_la = _input.LA(1);
			if ( !(_la==T__9 || _la==T__10) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(104);
			_la = _input.LA(1);
			if ( !(_la==T__11 || _la==T__12) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(105);
			match(ID);
			setState(106);
			match(T__1);
			setState(107);
			definicion_de_columna();
			setState(112);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(108);
				match(T__0);
				setState(109);
				definicion_de_columna();
				}
				}
				setState(114);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(115);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Eliminar_tablaContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SSqlParser.ID, 0); }
		public Eliminar_tablaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_eliminar_tabla; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).enterEliminar_tabla(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).exitEliminar_tabla(this);
		}
	}

	public final Eliminar_tablaContext eliminar_tabla() throws RecognitionException {
		Eliminar_tablaContext _localctx = new Eliminar_tablaContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_eliminar_tabla);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(117);
			_la = _input.LA(1);
			if ( !(_la==T__13 || _la==T__14) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(118);
			_la = _input.LA(1);
			if ( !(_la==T__11 || _la==T__12) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(119);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Lista_columnasContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(SSqlParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(SSqlParser.ID, i);
		}
		public Lista_columnasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lista_columnas; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).enterLista_columnas(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).exitLista_columnas(this);
		}
	}

	public final Lista_columnasContext lista_columnas() throws RecognitionException {
		Lista_columnasContext _localctx = new Lista_columnasContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_lista_columnas);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(121);
			match(ID);
			setState(126);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(122);
				match(T__0);
				setState(123);
				match(ID);
				}
				}
				setState(128);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Definicion_de_columnaContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SSqlParser.ID, 0); }
		public TerminalNode DEF_TIPO_DATO() { return getToken(SSqlParser.DEF_TIPO_DATO, 0); }
		public TerminalNode LLAVE_PRIMARIA() { return getToken(SSqlParser.LLAVE_PRIMARIA, 0); }
		public TerminalNode NULO() { return getToken(SSqlParser.NULO, 0); }
		public TerminalNode NO_NULO() { return getToken(SSqlParser.NO_NULO, 0); }
		public Definicion_de_columnaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_definicion_de_columna; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).enterDefinicion_de_columna(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).exitDefinicion_de_columna(this);
		}
	}

	public final Definicion_de_columnaContext definicion_de_columna() throws RecognitionException {
		Definicion_de_columnaContext _localctx = new Definicion_de_columnaContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_definicion_de_columna);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(129);
			match(ID);
			setState(130);
			match(DEF_TIPO_DATO);
			setState(132);
			_la = _input.LA(1);
			if (_la==NULO || _la==NO_NULO) {
				{
				setState(131);
				_la = _input.LA(1);
				if ( !(_la==NULO || _la==NO_NULO) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				}
			}

			setState(135);
			_la = _input.LA(1);
			if (_la==LLAVE_PRIMARIA) {
				{
				setState(134);
				match(LLAVE_PRIMARIA);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CondicionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SSqlParser.ID, 0); }
		public Tipo_datoContext tipo_dato() {
			return getRuleContext(Tipo_datoContext.class,0);
		}
		public List<CondicionContext> condicion() {
			return getRuleContexts(CondicionContext.class);
		}
		public CondicionContext condicion(int i) {
			return getRuleContext(CondicionContext.class,i);
		}
		public TerminalNode OPERADOR_LOGICO() { return getToken(SSqlParser.OPERADOR_LOGICO, 0); }
		public TerminalNode AND() { return getToken(SSqlParser.AND, 0); }
		public TerminalNode OR() { return getToken(SSqlParser.OR, 0); }
		public CondicionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condicion; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).enterCondicion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).exitCondicion(this);
		}
	}

	public final CondicionContext condicion() throws RecognitionException {
		return condicion(0);
	}

	private CondicionContext condicion(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		CondicionContext _localctx = new CondicionContext(_ctx, _parentState);
		CondicionContext _prevctx = _localctx;
		int _startState = 20;
		enterRecursionRule(_localctx, 20, RULE_condicion, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(144);
			switch (_input.LA(1)) {
			case ID:
				{
				setState(138);
				match(ID);
				}
				break;
			case ENTERO:
			case CADENA:
				{
				setState(139);
				tipo_dato();
				}
				break;
			case T__1:
				{
				setState(140);
				match(T__1);
				setState(141);
				condicion(0);
				setState(142);
				match(T__2);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(157);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(155);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
					case 1:
						{
						_localctx = new CondicionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_condicion);
						setState(146);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(147);
						match(OPERADOR_LOGICO);
						setState(148);
						condicion(5);
						}
						break;
					case 2:
						{
						_localctx = new CondicionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_condicion);
						setState(149);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(150);
						match(AND);
						setState(151);
						condicion(4);
						}
						break;
					case 3:
						{
						_localctx = new CondicionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_condicion);
						setState(152);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(153);
						match(OR);
						setState(154);
						condicion(3);
						}
						break;
					}
					} 
				}
				setState(159);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Tipo_datoContext extends ParserRuleContext {
		public TerminalNode ENTERO() { return getToken(SSqlParser.ENTERO, 0); }
		public TerminalNode CADENA() { return getToken(SSqlParser.CADENA, 0); }
		public Tipo_datoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tipo_dato; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).enterTipo_dato(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SSqlListener ) ((SSqlListener)listener).exitTipo_dato(this);
		}
	}

	public final Tipo_datoContext tipo_dato() throws RecognitionException {
		Tipo_datoContext _localctx = new Tipo_datoContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_tipo_dato);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(160);
			_la = _input.LA(1);
			if ( !(_la==ENTERO || _la==CADENA) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 10:
			return condicion_sempred((CondicionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean condicion_sempred(CondicionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 4);
		case 1:
			return precpred(_ctx, 3);
		case 2:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\'\u00a5\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\3\2\3\2\3\2\3\2\3\2\3\2\5\2!\n\2\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\7\3+\n\3\f\3\16\3.\13\3\5\3\60\n\3\3\3\3\3\3\3\7\3\65\n"+
		"\3\f\3\16\38\13\3\3\4\3\4\5\4<\n\4\3\5\3\5\3\5\3\5\3\5\3\5\7\5D\n\5\f"+
		"\5\16\5G\13\5\3\5\3\5\5\5K\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\7\6T\n\6\f"+
		"\6\16\6W\13\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\7\7c\n\7\f\7\16"+
		"\7f\13\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\7\bq\n\b\f\b\16\bt\13\b\3"+
		"\b\3\b\3\t\3\t\3\t\3\t\3\n\3\n\3\n\7\n\177\n\n\f\n\16\n\u0082\13\n\3\13"+
		"\3\13\3\13\5\13\u0087\n\13\3\13\5\13\u008a\n\13\3\f\3\f\3\f\3\f\3\f\3"+
		"\f\3\f\5\f\u0093\n\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\7\f\u009e\n\f"+
		"\f\f\16\f\u00a1\13\f\3\r\3\r\3\r\2\3\26\16\2\4\6\b\n\f\16\20\22\24\26"+
		"\30\2\f\3\2\36\37\3\2!\"\3\2\6\7\3\2\b\t\3\2\n\13\3\2\f\r\3\2\16\17\3"+
		"\2\20\21\3\2\32\33\3\2$%\u00ae\2 \3\2\2\2\4\"\3\2\2\2\6;\3\2\2\2\b=\3"+
		"\2\2\2\nL\3\2\2\2\fZ\3\2\2\2\16i\3\2\2\2\20w\3\2\2\2\22{\3\2\2\2\24\u0083"+
		"\3\2\2\2\26\u0092\3\2\2\2\30\u00a2\3\2\2\2\32!\5\4\3\2\33!\5\b\5\2\34"+
		"!\5\n\6\2\35!\5\16\b\2\36!\5\f\7\2\37!\5\20\t\2 \32\3\2\2\2 \33\3\2\2"+
		"\2 \34\3\2\2\2 \35\3\2\2\2 \36\3\2\2\2 \37\3\2\2\2!\3\3\2\2\2\"#\7\23"+
		"\2\2#$\5\6\4\2$%\7\35\2\2%/\7\22\2\2&\'\7\27\2\2\',\5\26\f\2()\t\2\2\2"+
		")+\5\26\f\2*(\3\2\2\2+.\3\2\2\2,*\3\2\2\2,-\3\2\2\2-\60\3\2\2\2.,\3\2"+
		"\2\2/&\3\2\2\2/\60\3\2\2\2\60\66\3\2\2\2\61\62\7 \2\2\62\63\7\22\2\2\63"+
		"\65\t\3\2\2\64\61\3\2\2\2\658\3\2\2\2\66\64\3\2\2\2\66\67\3\2\2\2\67\5"+
		"\3\2\2\28\66\3\2\2\29<\7\34\2\2:<\5\22\n\2;9\3\2\2\2;:\3\2\2\2<\7\3\2"+
		"\2\2=>\7\24\2\2>?\7\22\2\2?@\7\25\2\2@E\5\26\f\2AB\7\3\2\2BD\5\26\f\2"+
		"CA\3\2\2\2DG\3\2\2\2EC\3\2\2\2EF\3\2\2\2FJ\3\2\2\2GE\3\2\2\2HI\7\27\2"+
		"\2IK\5\26\f\2JH\3\2\2\2JK\3\2\2\2K\t\3\2\2\2LM\7\26\2\2MN\7\30\2\2NO\7"+
		"\22\2\2OP\7\4\2\2PU\5\24\13\2QR\7\3\2\2RT\5\24\13\2SQ\3\2\2\2TW\3\2\2"+
		"\2US\3\2\2\2UV\3\2\2\2VX\3\2\2\2WU\3\2\2\2XY\7\5\2\2Y\13\3\2\2\2Z[\t\4"+
		"\2\2[\\\t\5\2\2\\]\7\22\2\2]^\t\6\2\2^_\7\4\2\2_d\5\30\r\2`a\7\3\2\2a"+
		"c\5\30\r\2b`\3\2\2\2cf\3\2\2\2db\3\2\2\2de\3\2\2\2eg\3\2\2\2fd\3\2\2\2"+
		"gh\7\5\2\2h\r\3\2\2\2ij\t\7\2\2jk\t\b\2\2kl\7\22\2\2lm\7\4\2\2mr\5\24"+
		"\13\2no\7\3\2\2oq\5\24\13\2pn\3\2\2\2qt\3\2\2\2rp\3\2\2\2rs\3\2\2\2su"+
		"\3\2\2\2tr\3\2\2\2uv\7\5\2\2v\17\3\2\2\2wx\t\t\2\2xy\t\b\2\2yz\7\22\2"+
		"\2z\21\3\2\2\2{\u0080\7\22\2\2|}\7\3\2\2}\177\7\22\2\2~|\3\2\2\2\177\u0082"+
		"\3\2\2\2\u0080~\3\2\2\2\u0080\u0081\3\2\2\2\u0081\23\3\2\2\2\u0082\u0080"+
		"\3\2\2\2\u0083\u0084\7\22\2\2\u0084\u0086\7#\2\2\u0085\u0087\t\n\2\2\u0086"+
		"\u0085\3\2\2\2\u0086\u0087\3\2\2\2\u0087\u0089\3\2\2\2\u0088\u008a\7\31"+
		"\2\2\u0089\u0088\3\2\2\2\u0089\u008a\3\2\2\2\u008a\25\3\2\2\2\u008b\u008c"+
		"\b\f\1\2\u008c\u0093\7\22\2\2\u008d\u0093\5\30\r\2\u008e\u008f\7\4\2\2"+
		"\u008f\u0090\5\26\f\2\u0090\u0091\7\5\2\2\u0091\u0093\3\2\2\2\u0092\u008b"+
		"\3\2\2\2\u0092\u008d\3\2\2\2\u0092\u008e\3\2\2\2\u0093\u009f\3\2\2\2\u0094"+
		"\u0095\f\6\2\2\u0095\u0096\7&\2\2\u0096\u009e\5\26\f\7\u0097\u0098\f\5"+
		"\2\2\u0098\u0099\7\36\2\2\u0099\u009e\5\26\f\6\u009a\u009b\f\4\2\2\u009b"+
		"\u009c\7\37\2\2\u009c\u009e\5\26\f\5\u009d\u0094\3\2\2\2\u009d\u0097\3"+
		"\2\2\2\u009d\u009a\3\2\2\2\u009e\u00a1\3\2\2\2\u009f\u009d\3\2\2\2\u009f"+
		"\u00a0\3\2\2\2\u00a0\27\3\2\2\2\u00a1\u009f\3\2\2\2\u00a2\u00a3\t\13\2"+
		"\2\u00a3\31\3\2\2\2\22 ,/\66;EJUdr\u0080\u0086\u0089\u0092\u009d\u009f";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}