/**
 * 
 */
package org.basesdos.prototypedb.sql;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.basesdos.prototypedb.dbobject.Table;
import org.basesdos.prototypedb.exceptions.PrototypeDBException;
import org.basesdos.prototypedb.storage.impl.TableWriter;
//import org.basesdos.prototypedb.filemanagement.DbFileUtil;

/**
 * @author Sebastián Carabali (sebastiancc@unicauca.edu.co)
 *
 */
public class CreateTableStatement implements ISSqlStatement {

  private final Table table;

  public CreateTableStatement(Table table) {
    this.table = table;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.basesdos.prototypedb.sql.ISSqlStatement#execute()
   */
  @Override
  public String execute() {
      TableWriter tw = new TableWriter(table);
      try {
          tw.write();
          return "Tabla creada exisitosamente";
      } catch (IOException | PrototypeDBException ex) {
          Logger.getLogger(CreateTableStatement.class.getName()).log(Level.SEVERE, null, ex);
          return ex.getMessage();
      }
  }
}
