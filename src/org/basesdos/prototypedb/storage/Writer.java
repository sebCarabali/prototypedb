/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.basesdos.prototypedb.storage;

import java.io.IOException;
import org.basesdos.prototypedb.exceptions.PrototypeDBException;

/**
 * Los objetos que persisten deben implementar esta interfaz para guardarse.
 *
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public interface Writer {

    /**
     * Escribe en un flijo de salida.
     *
     * @throws IOException
     * @throws org.basesdos.prototypedb.exceptions.PrototypeDBException
     */
    public void write() throws IOException, PrototypeDBException;
}
