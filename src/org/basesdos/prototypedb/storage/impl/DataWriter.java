/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.basesdos.prototypedb.storage.impl;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;
import org.basesdos.prototypedb.dbobject.Table;
import org.basesdos.prototypedb.dbobject.TipoDatoLength;
import org.basesdos.prototypedb.exceptions.CantidadDatosNoConcuerdaException;
import org.basesdos.prototypedb.exceptions.PrototypeDBException;
import org.basesdos.prototypedb.storage.IoUtils;
import org.basesdos.prototypedb.storage.Writer;

/**
 * Persiste los datos de cada tabla en un archivo que tiene como nombre
 * < nombre_tabla >datos.data.
 *
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class DataWriter implements Writer {

    /**
     * Nombre de la tabla a insertar los valores.
     */
    private final String tableName;

    /**
     * Valores a insertar.
     */
    private final List<String> listaDatos;

    public DataWriter(String tableName, List<String> listaDatos) {
        this.tableName = tableName;
        this.listaDatos = listaDatos;
    }

    @Override
    public void write() throws IOException, PrototypeDBException {
        final TableReader tr = new TableReader(tableName);
        final Table tabla = (Table) tr.read();
        if (tabla.getListaColumnas().size() != listaDatos.size()) {
            throw new CantidadDatosNoConcuerdaException("La cantidad de datos no es igual a la cantidad de columnas.");
        }
        final RandomAccessFile raf = new RandomAccessFile("data/tables/" + tabla.getName() + "datos.data", "rw");
        raf.seek(raf.length());
        for (int i = 0; i < listaDatos.size(); i++) {
            final String data = listaDatos.get(i);
            if (data.matches("[0-9]+")) // Verificar si es un entero.
            {
                raf.writeInt(Integer.valueOf(data));
            } else if (listaDatos.get(i).startsWith("\"")) { // Verifica si es una cadena
                IoUtils.writeString(raf, data, TipoDatoLength.TAM_CADENA);
            } else { // Es real.
                raf.writeDouble(Double.valueOf(data));
            }
        }
    }
}
