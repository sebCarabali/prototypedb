package org.basesdos.prototypedb;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.basesdos.prototypedb.sql.PrototypeSqlListener;
import org.basesdos.prototypedb.sql.grammar.SSqlLexer;
import org.basesdos.prototypedb.sql.grammar.SSqlParser;

/**
 * @author Sebastián Carabali (sebastiancc@unicauca.edu.co).
 */
public class PrototypeDbFacade {

	public static String initSentencia(String sentencia) {
		//"CREAR TABLA prueba (id ENTERO LLAVE_PRIMARIA, nombre CADENA)"
		ANTLRInputStream input = new ANTLRInputStream(sentencia);

		SSqlLexer lexer = new SSqlLexer(input);

		CommonTokenStream commonTokenStream = new CommonTokenStream(lexer);

		SSqlParser parser = new SSqlParser(commonTokenStream);

		ParseTree tree = parser.sentencia();

		// System.out.println(tree.toStringTree());

		ParseTreeWalker walker = new ParseTreeWalker();

		PrototypeSqlListener listener = new PrototypeSqlListener();
		
		walker.walk(listener, tree);
		
		return listener.getMensaje();
	}
}
