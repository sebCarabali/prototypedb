package org.basesdos.prototypedb.interfaceDBMS;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import org.basesdos.prototypedb.PrototypeDbFacade;

public class WindowStart_DBMS extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2361674832357054647L;
	private JPanel contentPane;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WindowStart_DBMS frame = new WindowStart_DBMS();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public WindowStart_DBMS() {
		setTitle("PrototypeDBMS");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel_input = new JPanel();
		panel_input.setBounds(24, 23, 292, 129);
		contentPane.add(panel_input);
		panel_input.setLayout(null);
		
		JTextArea textArea = new JTextArea();		
		JScrollPane so = new JScrollPane(textArea);
		so.setBounds(10, 11, 272, 107);
		panel_input.add(so);
		
		JPanel panel_output = new JPanel();
		panel_output.setBounds(24, 163, 292, 73);
		contentPane.add(panel_output);
		panel_output.setLayout(null);
		
		JTextPane txtOutput = new JTextPane();
		txtOutput.setBounds(10, 5, 272, 57);
		txtOutput.setText("aqui van los resultados, errores etc");
		txtOutput.setEnabled(false);
		panel_output.add(txtOutput);
		
		JLabel lbl_input = new JLabel("Entrada");
		lbl_input.setBounds(24, 11, 46, 14);
		contentPane.add(lbl_input);
		
		JLabel lbl_output = new JLabel("Salida");
		lbl_output.setBounds(24, 151, 46, 14);
		contentPane.add(lbl_output);
		
		JPanel panel_btn_inputs = new JPanel();
		panel_btn_inputs.setBounds(326, 23, 81, 129);
		contentPane.add(panel_btn_inputs);
		
		JButton btnIniciar = new JButton("Iniciar");
		btnIniciar.setBounds(10, 11, 61, 23);
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				final String script=textArea.getText();
				final String mensaje = PrototypeDbFacade.initSentencia(script);
				txtOutput.setText(mensaje);
			}
		});
		panel_btn_inputs.setLayout(null);
		panel_btn_inputs.add(btnIniciar);
		
		JButton btn_Salir = new JButton("Salir");
		btn_Salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				setVisible(false);
				dispose();
			}
		});
		btn_Salir.setBounds(10, 95, 61, 23);
		panel_btn_inputs.add(btn_Salir);
	}
}
