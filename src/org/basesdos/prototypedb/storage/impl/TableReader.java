/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.basesdos.prototypedb.storage.impl;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import org.basesdos.prototypedb.dbobject.Column;
import org.basesdos.prototypedb.dbobject.DbObject;
import org.basesdos.prototypedb.dbobject.Table;
import org.basesdos.prototypedb.exceptions.PrototypeDBException;
import org.basesdos.prototypedb.storage.IoUtils;
import org.basesdos.prototypedb.storage.Reader;

/**
 * Lee la definición de una tabla desde el archivo de metadatos de esta.
 * 
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class TableReader implements Reader{

    private final String tableName;

    public TableReader(String tableName) {
        this.tableName = tableName;
    }
    
    @Override
    public DbObject read() throws IOException, PrototypeDBException {
        final Table dbObject = new Table();
        final RandomAccessFile raf = new RandomAccessFile("data/tables/"+tableName+".data", "rw");
        final ColumnReader cr = new ColumnReader(raf);
        final List<Column> lstCoumnas = new ArrayList<>();
        dbObject.setName(IoUtils.readString(raf, Table.TAM_NOMBRE));
        while(raf.getFilePointer() < raf.length())
        {
            final Column c = (Column) cr.read();
            lstCoumnas.add(c);
        }
        dbObject.setListaColumnas(lstCoumnas);
        return dbObject;
    }
}
