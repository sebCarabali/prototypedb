package org.basesdos.prototypedb.dbobject;

public enum TipoDato {
    ENTERO("ENTERO"),
    CADENA("CADENA"),
    REAL("REAL");

    String tipo;

    private TipoDato(String tipo) {
        this.tipo = tipo.toUpperCase();
    }

    public String getTipo() {
        return tipo;
    }
}
