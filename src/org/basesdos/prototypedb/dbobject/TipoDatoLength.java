/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.basesdos.prototypedb.dbobject;

/**
 *
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class TipoDatoLength 
{
    public static final int TAM_ENTERO = 8;
    
    public static final int TAM_REAL = 8;
    
    public static final int TAM_CADENA = 30;
}
