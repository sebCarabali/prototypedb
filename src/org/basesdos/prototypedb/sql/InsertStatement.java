/**
 *
 */
package org.basesdos.prototypedb.sql;

import java.io.IOException;
import java.io.RandomAccessFile;
import static java.nio.file.Files.list;
import static java.rmi.Naming.list;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.basesdos.prototypedb.dbobject.Column;
import org.basesdos.prototypedb.dbobject.ResultValue;
import org.basesdos.prototypedb.dbobject.Table;
import org.basesdos.prototypedb.exceptions.PrototypeDBException;
import org.basesdos.prototypedb.storage.Reader;
import org.basesdos.prototypedb.storage.Writer;
import org.basesdos.prototypedb.storage.ds.BPlusTree;
import org.basesdos.prototypedb.storage.impl.DataReader;
import org.basesdos.prototypedb.storage.impl.DataWriter;
import org.basesdos.prototypedb.storage.impl.Indexador;
import org.basesdos.prototypedb.storage.impl.TableReader;

/**
 * @author Sebastián Carabali (sebastiancc@unicauca.edu.co)
 *
 */
public class InsertStatement implements ISSqlStatement
{

    private List<String> valores;
    private String nametable;
    private RandomAccessFile raf;

    /*
   * (non-Javadoc)
   * 
   * @see org.basesdos.prototypedb.sql.ISSqlStatement#execute()
     */
    @Override
    public String execute()
    {
        Reader tableToRead = new TableReader(nametable);
        try 
        {
            Table table = (Table)tableToRead.read();
            List <Column> columns = table.getListaColumnas();             
            if(valores.size() == columns.size()){
                
                if(validateColumnPK(columns)){
                    BPlusTree<String, Long> index ;
                    openFile();
                    index = Indexador.indexarPorCodigo(raf,nametable);
                    closeFile();                    
                    int colPk = ColumnPk(columns);
                    validarPk(valores.get(colPk));
                }else{
                    Writer wd = new DataWriter(nametable,valores);
                    wd.write();
                }
               
            }else            {
                //llamado a la excepcion
            }
            
        } catch (IOException ex) {
            Logger.getLogger(InsertStatement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PrototypeDBException ex) {
            Logger.getLogger(InsertStatement.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*TableBuilder tblBuilder = new TableBuilder();
        tblBuilder.setName(ctx.ID().getText(nametable));
        for (SSqlParser.Definicion_de_columnaContext deffColCtx : ctx.definicion_de_columna()) {
            ColumnBuilder colBuilder = new ColumnBuilder();
            colBuilder.setName(deffColCtx.ID().getText());
            System.out.println("TIPO DE DATO: " + TipoDato.valueOf(deffColCtx.DEF_TIPO_DATO().getText()));
            colBuilder.setTipoDato(TipoDato.valueOf(deffColCtx.DEF_TIPO_DATO().getText()));
            colBuilder.setNullable((deffColCtx.NO_NULO() == null));
            colBuilder.setPrimaryKey((deffColCtx.LLAVE_PRIMARIA() != null));
            tblBuilder.addColumn(colBuilder.build());
        }
        CreateTableStatement createStmt = new CreateTableStatement(tblBuilder.build());
        mensaje = createStmt.execute();*/
        return null;
    }
    
    public void addValor(String valor)
    {
        if(valores == null) {
            valores = new ArrayList<>();
        }
        valores.add(valor);
    }

    private boolean validarNombreTabla(String tableName)
    {

        return false;
    }

    private int ColumnPk(List<Column> c) 
    {
        int iterator = 0;
        while(iterator <= c.size())
        {
            if(c.get(iterator).isPrimaryKey())
            {
                return iterator;
            }
        }
        return 0;
    }

    private void validarPk(Object get) throws IOException, PrototypeDBException {
        //abrimos el archivo
        openFile();
        Reader red = new DataReader(nametable);
        List<ResultValue> lv = (List<ResultValue>)red.read();
        for(ResultValue rv : lv){
        closeFile();
        }
    }
    
     private void openFile() throws IOException
    {
        raf = new RandomAccessFile("data/tables/" + nametable + "datos.data", "rw");
    }
    
    private void closeFile() throws IOException
    {
        if(raf != null)
            raf.close();
        raf = null;
    }
    
    public void setNameTable(String nametable)
    {
        this.nametable = nametable;
    }

    private boolean validateColumnPK(List<Column> columns) {
        for (Column col : columns)
        {
            if(col.isPrimaryKey()){ return true;}
        }
        return false;
    }

}
