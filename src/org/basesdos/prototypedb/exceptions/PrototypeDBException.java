/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.basesdos.prototypedb.exceptions;

/**
 *
 * @author Sebastián Carabali(sebastiancc@unicauca.edu.co)
 */
public class PrototypeDBException extends Exception
{

    public PrototypeDBException(String mensaje) {
        super(mensaje);
    }
     
}
